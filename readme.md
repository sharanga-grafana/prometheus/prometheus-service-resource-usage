**Process/Service resource usage in Grafana**
With the help of this script you can install node exporter and forward the desired service resource usage to promethues to display it on grafana dashboard
*Note:* This script is only made to run with **node_exporter**

![Screenshot](https://gitlab.com/sharanga-grafana/prometheus/prometheus-service-resource-usage/-/raw/main/screenshot.png)

This script is distributed into 3 sections:
1. Install latest version of node exporter
2. Configure node exporter service
3. Setup process resouce usage script on the server.

You may add/edit the process/service in this script to get the desired results.
