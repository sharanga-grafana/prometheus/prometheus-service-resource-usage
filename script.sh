#Install node exporter
version=$(curl -s https://api.github.com/repos/prometheus/node_exporter/releases/latest |  grep "tag_name" | cut -d : -f 2 | tr -d v,\" | xargs)
cd /tmp/ && wget -q https://github.com/prometheus/node_exporter/releases/download/v"$version"/node_exporter-"$version".linux-amd64.tar.gz -O node_exporter.tar.gz
tar -xf node_exporter.tar.gz
cp node_exporter-"$version".linux-amd64/node_exporter /usr/local/bin
sudo useradd --no-create-home --shell /bin/false node_exporter
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
rm -rf node_exporter*

#Configure node exporter
cat <<EOF > /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter --collector.systemd  --collector.textfile.directory=/var/lib/node_exporter/textfile_collector

[Install]
WantedBy=multi-user.target
EOF

#Node exporter process resource usage script
mkdir -p /var/lib/node_exporter/textfile_collector/
cat <<\REDLINE > /root/process-usage.sh
#!/bin/bash

TEXTFILE_COLLECTOR_DIR=/var/lib/node_exporter/textfile_collector/

#Nginx
nginx_pid=$(ps -A | grep nginx | awk 'FNR == 1 {print}' | awk '{print $1}')
nginx_mem=$(ps -eo %mem,cmd | grep nginx | awk '{print $1}' | paste -sd+ | bc)
nginx_cpu=$(ps -eo %cpu,cmd | grep nginx | awk '{print $1}' | paste -sd+ | bc)

#PHP-FPM
php_pid=$(ps -A | grep php | awk 'FNR == 1 {print}' | awk '{print $1}')
php_mem=$(ps -eo %mem,cmd | grep php | awk '{print $1}' | paste -sd+ | bc)
php_cpu=$(ps -eo %cpu,cmd | grep php | awk '{print $1}' | paste -sd+ | bc)

#Mysql
mysql_pid=$(ps -A | grep mysql | awk 'FNR == 1 {print}' | awk '{print $1}')
mysql_mem=$(ps -eo %mem,cmd | grep mysql | awk '{print $1}' | paste -sd+ | bc)
mysql_cpu=$(ps -eo %cpu,cmd | grep mysql | awk '{print $1}' | paste -sd+ | bc)

#Node-Exporter
node_exporter_pid=$(ps -A | grep node_exporter | awk 'FNR == 1 {print}' | awk '{print $1}')
node_exporter_mem=$(ps -eo %mem,cmd | grep node_exporter | awk '{print $1}' | paste -sd+ | bc)
node_exporter_cpu=$(ps -eo %cpu,cmd | grep node_exporter | awk '{print $1}' | paste -sd+ | bc)

#Redis-Server
redis_server_pid=$(ps -A | grep redis-server | awk 'FNR == 1 {print}' | awk '{print $1}')
redis_server_mem=$(ps -eo %mem,cmd | grep redis-server | awk '{print $1}' | paste -sd+ | bc)
redis_server_cpu=$(ps -eo %cpu,cmd | grep redis-server | awk '{print $1}' | paste -sd+ | bc)

cat << EOF > "$TEXTFILE_COLLECTOR_DIR/process_usage.prom.$$"
#Custom Dash
#1) Nginx
node_systemd_unit_pid{Service="Nginx"} $nginx_pid
node_systemd_unit_mem{Service="Nginx"} $nginx_mem
node_systemd_unit_cpu{Service="Nginx"} $nginx_cpu
#2) PHP-FPM
node_systemd_unit_pid{Service="PHP-FPM"} $php_pid
node_systemd_unit_mem{Service="PHP-FPM"} $php_mem
node_systemd_unit_cpu{Service="PHP-FPM"} $php_cpu
#3) Mysql
node_systemd_unit_pid{Service="Mysql"} $mysql_pid
node_systemd_unit_mem{Service="Mysql"} $mysql_mem
node_systemd_unit_cpu{Service="Mysql"} $mysql_cpu
#4) Node_Exporter
node_systemd_unit_pid{Service="Node-Exporter"} $node_exporter_pid
node_systemd_unit_mem{Service="Node-Exporter"} $node_exporter_mem
node_systemd_unit_cpu{Service="Node-Exporter"} $node_exporter_cpu
#5) Redis-Server
node_systemd_unit_pid{Service="Redis-Server"} $redis_server_pid
node_systemd_unit_mem{Service="Redis-Server"} $redis_server_mem
node_systemd_unit_cpu{Service="Redis-Server"} $redis_server_cpu
EOF

mv "$TEXTFILE_COLLECTOR_DIR/process_usage.prom.$$" \
  "$TEXTFILE_COLLECTOR_DIR/process_usage.prom"
REDLINE

chmod +x /root/process-usage.sh
sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl enable node_exporter
